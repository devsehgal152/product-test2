<!DOCTYPE html>
<html lang="en">
<head>
  <title>Products</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"></script>
@notify_css
<style>
    .invalid-feedback {
        display: block;
        width: 100%;
        margin-top: -0.75rem;
        font-size: 0.8em;
        color: #000000;
    }
</style>
</head>
<body style="background-color:grey">
<div class="container">
    @yield('content')
</div>
@notify_js
@notify_render
</body>
</html>