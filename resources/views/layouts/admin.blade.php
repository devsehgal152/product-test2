<!DOCTYPE html>
<html lang="en">
<head>
  <title>Products</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"
    integrity="sha512-AA1Bzp5Q0K1KanKKmvN/4d3IRKVlv9PYgwFPvm32nPO6QS8yH1HO7LbgB1pgiOxPtfeg5zEn2ba64MUcqJx6CA=="
    crossorigin="anonymous"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
@notify_css
<style>
    .invalid-feedback {
        display: block;
        width: 100%;
        margin-top: -0.75rem;
        font-size: 0.8em;
        color: red;
    }
    .nav-link.active{
        background-color: #198754;
        border-radius: 10px;
    }
    
</style>
</head>
<body>
<div class="container">
    @include('includes.navbar')
    <div class="container mt-5 pt-5">
        @yield('content')
    </div>
</div>
@notify_js
@notify_render
<script>
$(document).ready(function(){
    $('body').on('click', '.delete-confirm', function() {
        var formId = $(this).attr('data-form');
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            buttons: ["Cancel", "Yes"],

        }).then(function(value) {
                if (value == true) {
                    $(`form#${formId}`).submit();
                } else {
                    location.reload();
                }
        });
    });
});
</script>
@yield('extra-scripts')
</body>
</html>