@extends('layouts.admin')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-10">
            <h5>Products</h5>
        </div>
        <div class="col-lg-2">
            <a href="{{route('products.create')}}" class="btn btn-primary">Add Product</a>
        </div>
    </div>
    <div class="row">
        <table class="table table-light table-striped">
            <thead>
            <tr>
                <th>Name</th>
                <th>Sale Price</th>
                <th>Actual Price</th>
                <th>Created at</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
                @forelse($data as $val)
                <tr>
                    <td>{{$val->name}}</td>
                    <td>{{$val->sell_price}}</td>
                    <td>{{$val->actual_price}}</td>
                    <td>{{$val->created_at}}</td>
                    <td>
                        <a href="{{ route('products.edit', [base64_encode($val->id)]) }}"
                            class="btn btn-primary btn-sm">Edit</a>
                        <a href="javascript:void(0)" class="btn btn-danger btn-sm delete-confirm"
                            data-form="deleteForm-{{ $val->id }}">Delete</a>
                        <form id="deleteForm-{{ $val->id }}"
                            action="{{ route('products.destroy', [base64_encode($val->id)]) }}" method="post">
                            @csrf @method('DELETE')
                        </form>
                    </td>  
                </tr>
                @empty
                <tr>
                    <td colspan="5">No Products</td>
                </tr>
                @endforelse
            </tbody>
        </table>
    </div>
    <div class="row">
        {!! $data->links() !!}
    </div>
</div>
@endsection

