@extends('layouts.admin')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-10">
            <h5>Edit Prodcut</h5>
        </div>
        <div class="col-lg-2">
            <a href="{{route('products.index')}}" class="btn btn-primary">Back</a>
        </div>
    </div>
    <div class="row">
        <form method="POST" enctype="multipart/form-data" action="{{route('products.update',$data->id)}}">
            @csrf
            @method('PUT')
            <div class="mb-3 mt-3">
                <label for="email" class="form-label">Name:</label>
                <input type="text" class="form-control" required id="email" placeholder="Enter Name" value="{{old('name',$data->name)}}" name="name">
            </div>   
            @error('name')
                <span class="validation invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror 
            <div class="mb-3 mt-3">
                <label for="email" class="form-label">Sale Price:</label>
                <input type="number" min="1" step="any" max="10000" class="form-control" required id="" placeholder="Enter Sale Price" value="{{old('sell_price',$data->sell_price)}}" name="sell_price">
            </div>   
            <div class="mb-3 mt-3">
                <label for="email" class="form-label">Actual Price:</label>
                <input type="number" min="1" step="any" max="10000" class="form-control" required id="" placeholder="Enter Question" value="{{old('actual_price',$data->actual_price)}}" name="actual_price">
            </div>   
            @foreach($data->images as $img)
                <div class="row">
                    <div class="mt-2">
                        <figure style="height:200px"><img  style="height:200px" src="{{$img->image_url}}" /></figure>
                        <a href="{{route('products.imageDelete',$img->id)}}"  class=" mt-2 btn btn-primary" title="">Delete</a>
                    </div>
                </div>
            @endforeach
            <div class="row"><h5>Add Images</h5></div>   
                <div class="field_wrapper">
                    <div class="row">
                        <div class="mt-2">
                            <input type="file"  class="form-control" name="image[]" accept="image/*"/>
                            <a href="javascript:void(0);"  class="add_button mt-2 btn btn-primary" title="Add field">Add more</a>
                        </div>
                    </div>
                </div>
            @foreach($data->varients as $key=>$var)
                <div class="row">
                    <div class="mt-2">
                        <input type="hidden" name="varient[{{$key}}][id]"  value="{{$var->id}}"/>
                        <label for="email" class="form-label">Name:</label>
                        <input type="text" class="form-control"  name="varient[{{$key}}][name]" value="{{$var->name}}"/>
                        <label for="email" class="form-label">Value:</label>
                        <input type="text" class="form-control"  name="varient[{{$key}}][value]" value="{{$var->value}}"/>
                        <a href="{{route('products.varientDelete',$var->id)}}"  class=" mt-2 btn btn-primary" title="">Delete</a>
                    </div>
                </div>
            @endforeach
            <div class="row mt-5"><h5>Add Varient</h5></div>
                <div class="field_wrapper2">
                    <div class="row">
                        <div class="mt-2">
                            <input type="hidden" name="varient[{{count($data->varients)}}][id]"  value="0"/>
                            <label for="email" class="form-label">Name:</label>
                            <input type="text" class="form-control"  name="varient[{{count($data->varients)}}][name]" value=""/>
                            <label for="email" class="form-label">Value:</label>
                            <input type="text" class="form-control"  name="varient[{{count($data->varients)}}][value]" value=""/>
                            <a href="javascript:void(0);" class="add_button2 btn btn-primary mt-2" title="Add field">Add more</a>
                        </div>
                    </div>
                </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
@endsection
@section('extra-scripts')
<script type="text/javascript">
$(document).ready(function(){
    var maxField = 10; //Input fields increment limitation
    var addButton = $('.add_button'); //Add button selector
    var wrapper = $('.field_wrapper'); //Input field wrapper
    var fieldHTML = '<div class="row"><div class="mt-2"><input type="file" class="form-control" accept="image/*" name="image[]"/><a href="javascript:void(0);" class="remove_button btn btn-warning">Remove</a></div></div>'; //New input field html 
    var x = 1; //Initial field counter is 1
    
    //Once add button is clicked
    $(addButton).click(function(){
        //Check maximum number of input fields
        if(x < maxField){ 
            x++; //Increment field counter
            $(wrapper).append(fieldHTML); //Add field html
        }
    });
    
    //Once remove button is clicked
    $(wrapper).on('click', '.remove_button', function(e){
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });




    var maxField2 = 10; //Input fields increment limitation
    var addButton2 = $('.add_button2'); //Add button selector
    var wrapper2 = $('.field_wrapper2'); //Input field wrapper
    var x2 = {{count($data->Varients)}}+1; //Initial field counter is 1
    var fieldHTML2 = '<div class="row"><div class="mt-2"><input type="hidden" name="varient['+x2+'][id]"  value="0"/><label for="email" class="form-label">Name:</label><input type="text" class="form-control" name="varient['+x2+'][name]" value=""/><label for="email" class="form-label">Value:</label><input type="text" class="form-control" name="varient['+x2+'][value]" value=""/><a href="javascript:void(0);" class="remove_button2 mt-2 btn btn-warning" title="Add field">Remove</a></div></div>'; //New input field html 
    
    
    //Once add button is clicked
    $(addButton2).click(function(){
        //Check maximum number of input fields
        if(x2 < maxField2){ 
            x2++; //Increment field counter
            $(wrapper2).append(fieldHTML2); //Add field html
        }
    });
    
    //Once remove button is clicked
    $(wrapper2).on('click', '.remove_button2', function(e){
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x2--; //Decrement field counter
    });

});
</script>
@endsection
