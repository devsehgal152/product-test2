<!-- A grey horizontal navbar that becomes vertical on small screens -->
<nav class="navbar navbar-expand-sm bg-dark navbar-dark fixed-top mb-5">
  <div class="container">
    <ul class="navbar-nav" style="display: contents">
        <li class="nav-item">
            <a class="nav-link {{ request()->segment(1) == 'view-products' ?'active':''}}" href="{{route('userProduct.index')}}">Products</a>
        </li>
    </ul>
  </div>
</nav>