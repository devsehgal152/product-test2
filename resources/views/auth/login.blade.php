@extends('layouts.auth')
@section('content')
    <div class="row justify-content-center mt-5">
        <div class="col-md-6">
            <form action="{{route('login')}}" method="POST">
                @csrf
                <div class="mb-3 mt-3">
                    <label for="email" class="form-label">Email:</label>
                    <input type="email" class="form-control" required id="email" placeholder="Enter email" name="email" value="{{old('email')}}">
                </div>
                <div class="mb-3">
                    <label for="pwd" class="form-label">Password:</label>
                    <input type="password" class="form-control" required min="8" id="pwd" placeholder="Enter password" name="password">
                </div>
                @error('password')
                    <span class="validation invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
    <div class="row justify-content-center mt-5">
        <div class="col-lg-4">
          <a type="button" class="btn btn-primary" href="{{route('userProduct.index')}}">View Products</a>
        </div>
    </div>
@endsection
