@extends('layouts.user')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-6">
            <h4>Prodcut Detail</h4>
        </div>
        <div class="col-lg-4">
        </div>
        <div class="col-lg-2">
            <a href="{{route('userProduct.index')}}" class="btn btn-primary">Back</a>
        </div> 
    </div>
    <div class="row">
        <h4> Name : {{$data->name}}</h4>
        <h5> Price : $ {{$data->sell_price}} <s> $ {{$data->actual_price}}</s></h5>
    </div>
    <div class="row mt-3 d-flex">
        <h5 class="mb-3">Images :-</h5>
        @foreach($data->images as $img)
            <div class="mt-2">
                <figure style="height:200px"><img  style="height:200px" src="{{$img->image_url}}" /></figure>
            </div>
        @endforeach
    </div>
    <div class="row mt-3 d-flex">
        <h5 class="mb-3 mt-2">Varients :-</h5>
        @foreach($data->varients as $key=>$var)        
            <h5 class="mb-3 mt-2">Varients {{$key+1}} :-</h5>
            <div class="mt-2">
                <h5 class="pl-5"> Name : {{$var->name}}</h5>
                <h5 class="pl-5"> Value : {{$var->value}}</h5>
            </div>
        @endforeach
    </div>
</div>
@endsection