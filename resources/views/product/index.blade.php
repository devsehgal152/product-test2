@extends('layouts.user')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-6">
            <h4> Prodcuts</h4>
        </div>
        <div class="col-lg-4">
        </div>
        <div class="col-lg-2">
            <a href="{{route('login')}}" class="btn btn-primary">Back</a>
        </div> 
    </div>
    <div class="row">
    @forelse($data as $val)
        <div class="card" style="width: 18rem;">
            @if(count($val->images)> 0)
                <img src="{{$val->images[0]->image_url}}" class="card-img-top" alt="...">
            @endif
            <div class="card-body">
                <h5 class="card-title">{{$val->name}}</h5>
                <p class="card-text">$ {{$val->sell_price}}</p>
                <p class="card-text"><s> $ {{$val->actual_price}}</s></p>
                <a href="{{route('userProduct.detail',base64_encode($val->id))}}" class="btn btn-primary">View details</a>
            </div>
        </div>
    @empty
    <div class="row">
        <h5>No Products</h5>
    </div>
    @endforelse
    </div>
    <div class="row">
        {!! $data->links() !!}
    </div>
</div>
@endsection