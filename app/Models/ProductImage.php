<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    use HasFactory;
    protected $guarded = ['id'];
     protected $appends = [
        'image_url'
    ];
    
    public function getImageUrlAttribute()
    {
        if (preg_match('(https://|http://)', $this->image) === 1) {
            return $this->image;
        }
        return !empty($this->image) ? asset("uploads/product/$this->image") : asset('assets/img/user-icon.png');
    }


}
