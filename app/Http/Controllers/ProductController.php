<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;

class ProductController extends Controller
{
    public function index(){

        $data = Product::with('images','varients')->latest()->paginate(5);
        // return $data;
        return view('product.index',compact('data'));
    
    }

    public function getProduct($id){

        $data = Product::with('images','varients')->find(base64_decode($id));
        // return $data;
        return view('product.detail',compact('data'));

    }
}
