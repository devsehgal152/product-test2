<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\ProductVarient;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   public function index()
    {
        
        $data = Product::with('images','varients')->latest()->paginate(10);
        return view('admin.product.index',compact('data'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function create(Request $request)
    {
        return view('admin.product.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'    => 'required',
            'actual_price' => 'required',
            'sell_price' => 'required'
        ]);

        // return $request->all();

        $data = $request->except(['_token', '_method','image','varient']);
       
        $product = Product::create($data);
        
            foreach($request->image as $proImage){
                    
                $proImageData['product_id'] =  $product->id;                
                $proImageData['image'] = $this->fileUpload($proImage, 'product')['name'] ?? null;


                ProductImage::create($proImageData);

            } 
            
            foreach($request->varient as $val){
                    
                $proData['product_id'] =  $product->id;
                $proData['name'] =  $val['name'];
                $proData['value'] =  $val['value'];
                
                ProductVarient::create($proData);

            }
  
        notify()->success('Product Added Successfully');
        return redirect()->route('products.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $data = Product::with('images','varients')->find(base64_decode($id));
        // return $data;
        return view('admin.product.edit', compact('data'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([
            'name'    => 'required',
            'actual_price' => 'required',
            'sell_price' => 'required'
        ]);

        $data = $request->except(['_token', '_method','image','varient']);

        $product = Product::find($id);
        $product->update = $data;
        
        foreach($request->image as $proImage){
                
            $proImageData['product_id'] =  $product->id;
            $proImageData['image'] = $this->fileUpload($proImage, 'product')['name'] ?? null;

            ProductImage::create($proImageData);

        } 
            
        foreach($request->varient as $val){
                
            $proData['product_id'] =  $product->id;
            $proData['name'] =  $val['name'];
            $proData['value'] =  $val['value'];
            
            if($val['id']>0){
                ProductVarient::updateOrCreate(
                    [
                         'id'=> $val['id']
                    ],
                $proData
                );
            }
            else{
                ProductVarient::create($proData);
            }
        }

        notify()->success('Product successfuly updated');
        return redirect()->back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        Product::destroy(base64_decode($id));        
        ProductImage::where('product_id',base64_decode($id))->delete();
        ProductVarient::where('product_id',base64_decode($id))->delete();

        notify()->success('Product deleted successfully');
        return back();
        
    }

    public function imageDelete($id){

        ProductImage::destroy($id);
        notify()->success('Product image deleted successfully');
        return redirect()->back();

    }
    
    public function varientDelete($id){

        ProductVarient::destroy($id);
        notify()->success('Product varient deleted successfully');
        return redirect()->back();

    }


}
