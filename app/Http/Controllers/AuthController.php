<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\User;

class AuthController extends Controller
{
    public function loginForm()
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required|min:8'
        ]);

        $rememberMe = $request->remember ? true : false;

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password], $rememberMe)) {
            
            if(Auth::user()->isAdmin()){
            
                if (!empty($request->next)) {
                    return redirect($request->next);
                }
                return redirect()->route('products.index');
           
            }
            Auth::logout();

            notify()->warning('Invalid credentails');
            return redirect()->route('login')->withInput($request->only('email', 'remember'));
       

        }
        notify()->warning('Invalid credentails'); 
        return redirect()->route('login')->withInput($request->only('email', 'remember'));
    }

    
    public function logout()
    {
        if (Auth::check()) {
            Auth::logout();
            session()->flush();
            notify()->success('Logged out successfully');
            return redirect(route('loginForm'));
        }
        notify()->success('Somthing went wrong');
        return redirect(route('loginForm'));
    }

}