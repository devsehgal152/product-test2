<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    
    return redirect(route('loginForm'));

})->name('home');

Route::namespace('App\Http\Controllers')->group(function() {


    Route::get('login', 'AuthController@loginform')->name('loginForm');
    Route::post('login', 'AuthController@login')->name('login');
    Route::get('logout', 'AuthController@logout')->name('logout');

    Route::middleware(['admin'])->namespace('Admin')->group(function () {

        Route::resource('products', 'ProductController');
        Route::get('imageDelete/{id}', 'ProductController@imageDelete')->name('products.imageDelete');
        Route::get('varientDelete/{id}', 'ProductController@varientDelete')->name('products.varientDelete');

    });
    
    Route::get('view-products', 'ProductController@index')->name('userProduct.index');
    Route::get('product-detail/{id}', 'ProductController@getProduct')->name('userProduct.detail');

});


